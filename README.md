# Valheim setup

Valheim dedicated server and client side mod setup.

Within this document the `client` refers to you and the `server` corresponds to the dedicated game server.

## How to join the dedicated server
You can join directly by IP `valheim.keckj.fr` by using the in-game server browser. You may also add  `+connect valheim.keckj.fr:2456` to your steam launch options to allow the game to autoconnect. To be able to connect you will need an up to date game version as well as some matching mod versions (ValheimPlus). The server is password protected.

## How to enable console and improve game performances
Add `-screen-fullscreen -window-mode exclusive -console` to your Valheim game launch options in steam. 

## How to install all client mods at once on Windows
This repository provides all mods already unpacked and configured as a copy-pastable folder for easy installation. 
Before doing anything, create a backup of your worlds and characters just in case something breaks. Copy the directory `%appdata%\..\LocalLow\IronGate\Valheim` somewhere else on your computer. You can now proceed to mod installation:
1. Install git from this [link](https://git-scm.com/download/win), 64-bit Git for Windows Setup. Default installer options are good so just click next until done.
2. Open Windows Powershell, and type the following (use tab-completion):
    ```
    cd Documents
    git clone --recurse-submodules https://gitlab.com/keckj/valheim.git
    cd valheim\Valheim
    ii .
    ```
    This should clone this repository which contains all required mod files inside your `$HOME/Documents/valheim` folder.  Copy all the files contained in the Valheim folder (this folder automatically opens when you type in the last command).
3. Determine your steam game folder directly from within your steam game library (right click Valheim - properties - local files - browse files), most likely you will get to `C:\Program Files (x86)\Steam\steamapps\common\Valheim` unless you set a custom install game folder.
4. Just paste previously copied content directly into your Valheim folder (determined at previous step). If prompted overwrite all files. Some mods will create a configuration file on first game launch.
5. Start the game without joining the server and quit. You may now tweak client side mod configuration files, see the list of provided mods for details and adviced changes.
6. Restart the game and connect to the server to finally enjoy an authentic hassle-free viking life !

## How to update the mods after a first installation
1. Open Windows Powershell, and type the following (use tab-completion):
    ```
    cd Documents\valheim
    git pull
    git submodule update --init --recursive
    cd Valheim
    ii .
    ```
2. Overwrite the content of `C:\Program Files (x86)\Steam\steamapps\common\Valheim` with the files contained in the now up to date `Valheim` directory.

## List of provided mods:
1. **ValheimPlus v0.9.6**: Client/server mod that provides many quality of life improvements, you can have an overview [here](https://valheim.plus/). The mod can be downloaded [here](https://github.com/valheimPlus/ValheimPlus).
The V+ mod configuration file is enforced by the server so there is no client-side parameters to tweak.
2. **Equipment and quick slots v2.0.x**: Give equipped items their own dedicated inventory slots, plus three more hotkeyable quick slots. You can configure custom hotkeys in `Valheim\BepInEx\config\randyknapp.mods.equipmentandquickslots.cfg`. If you are playing with an azerty keyboard, remap the `Z` key to `W`. You may also remap `V` and `B` to  `B` and `N` if you are already using `V` as your push-to-talk key. 
3. **Better UI**: Updates the game UI with few subtle edits. By default every modification is turned ON. You may not like all of them, so you have the option to toggle certain modifications. To toggle these options, please modify the config file `Valheim\Bepinex\config\MK_BetterUI.cfg`. Set `showCharacterXP = false` to disable the ugly bottom yellow bar and `enemyLvlStyle = 2` to enable star icons on enemies.
4. **Clock**: Adds a simple configurable clock widget to the screen showing the in-game time of day or a fuzzy representation like "Late Afternoon" and optionally the current day. Clock position, size, color and font are configuratable in `Valheim\Bepinex\config\Maedenthorn.ClockMod.cfg`, default config is ok.
5. **Planting Plus**: This mod will allow you to place berry bushes, mushrooms, thistle, dandelion, birch trees, oak trees, and ancient trees by planting their respective resources with the cultivator. Please do not modify the configuration file `Valheim/BepInEx/config/com.bkeyes93.PlantingPlus.cfg`.
6. **Better archery**: Adds retrievable arrows, zooming while drawing the bow, aime improvements, arrow physics improvements and a craftable quiver that can hold up to three different type of arrows (shortcut alt + 1-3 by default). Configurable in `Valheim/BepInEx/config/ishid4.mods.betterarchery.cfg`.
7. **Instant monster loop drop**: Causes monsters to drop their loot and poof immediately.  
8. **Slope hitreg fix**: This mod uncaps the Y angle of your attacks so you can now look straight down or up and hit things if they are in range.
9. **Massfarming**: Provides a hotkey to mass-pickup and mass plant. Hold left shift while planting to plant on a grid / harvest many plants at once. 
10. **Skill loss mod**: Removes the 5% Skill level loss on death.
11. **Craftable chain**: Adds a crafting recipe for the chain. Needs 2 iron and a forge at level 4. 
12. **A little help**: Makes it a little easier to get back to your body if you keep dying. 
13. **Dodge on double tap**: This mod gives you the ability to do a dodge roll by double tapping in either direction.
14. **Harpoons plus**: Improves harpoon functionality allowing the user to customize all aspects of the harpoon.
15. **Cheaper stone pieces**: This mod adjusts the building costs of stone building pieces to be relative to their size. On average building, stone structures will now be cheaper than before, but better balanced. 
16. **Skill injector**: Injects new skills in a simple way for mod authors. 
17. **Sailing skill**: Adds a sailing skill to the game.
18. **Cooking skill**: Introduces a new Cooking Skill that buffs your food as it levels.
19. **Fitness skill**: Adds a Fitness skill to the game. This increases your maximum base stamina and regen and is increased by using stamina. 
20. **Use equipment in water**: Allows you to use your equipment while swimming in water.
21. **Teleporter title**: Show the teleporter name on the center of the screen. 
22. **Usefull path**: Players run, jog, and walk faster and use less stamina when traversing leveled ground.
23. **Fishing overhaul**: Allows you to catch fish in water with your hands if they get too close.
24. **Player stats in range**: PlayerStats will show health data for all nearby players within a given range of you. 
25. **Simple sort**: Allows you to sort your inventory. Ascending sort `Left Alt + {n,w,v}` and descending sort `Left Ctrl + {n,w,v}` where `n` sorts by name, `w` by weight and `v` by value.
26. **InSlimVML**: `*.dll` splicing tool for modding Unity packaged games (alternative mod loader compatible with bepinex). 
27. **Buildshare advanced building**: Save and load constructions using the file format `*.vbuild`. 
28. **Teleport Wolves**: Makes your wolves able to follow you through portals. 


## Buildshare buildings:
- **Boar Machine**: Boar-Breeding Tower 
- **Wolf breeding tower**: A Tower with the sole purpose of breeding wolves continuously
- **Reflx portal room**: Simple but aesthetic portal room. 
- **Storage matrix**: A compact and efficient storage facility.
- **Pipgen cage greenhouse**: Containtment Pen / Cage - Greenhouse 
- **Stair case 16 meters**: Staircaises
- **Miko's tower**: Comfort 15 bedroom for 6 people
- **Floki's viking longhouse**: Viking longhouse - 13x9 with crafting area, bedroom, 2 hearths and bank. 
- **Floki's dock**: A boat dock for shallow water.
- **Dragon's reach house**: A 4 story player house loosely based on Dragon's Reach from Skyrim. 


## Mods under consideration
- A mod that modifies the way portal work
