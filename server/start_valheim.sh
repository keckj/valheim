export templdpath=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=./linux64:$LD_LIBRARY_PATH
export SteamAppId=892970

# Upgrade valheim server
echo "Checking for updates..."
/home/steam/steamcmd +login anonymous +force_install_dir /home/steam/valheim +app_update 896660 +quit

# Tip: Make a local copy of this script to avoid it being overwritten by steam.
# NOTE: Minimum password length is 5 characters & Password cant be in the server name.
# NOTE: You need to make sure the ports 2456-2458 is being forwarded to your server through your local router & firewall.
echo "Starting valheim plus server..."
./start_server_bepinex.sh -name "La confrerie des loutres" -port 2456 -world "GROOTYWORLD" -public 1 -password "password"

echo "Server started."
export LD_LIBRARY_PATH=$templdpath

while :
do
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')
echo "valheim.service: timestamp ${TIMESTAMP}"
sleep 60
done
